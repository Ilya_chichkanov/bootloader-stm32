#include "flash.h"
//extern UART_HandleTypeDef huart2;
uint32_t program_page(const uint8_t *data,const uint16_t page,const uint16_t pack_size)
{

	  /* Unlock the Flash to enable the flash control register access *************/
	  HAL_FLASH_Unlock();
	  /* Allow Access to option bytes sector */
	  HAL_FLASH_OB_Unlock();
	  /* Fill EraseInit structure*/
	  FLASH_EraseInitTypeDef EraseInitStruct;
	  EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
	  EraseInitStruct.Banks = FLASH_BANK_1;
	  EraseInitStruct.Page = page;
	  EraseInitStruct.NbPages = 1;
	  uint32_t PageError;

	  volatile uint32_t write_cnt=0, index=0;
	  volatile HAL_StatusTypeDef status;

	  status = HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);
	  if(status != HAL_OK)
	  {
		  return PageError;
	  }

	  while(index < pack_size/8)
	  {

		  status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, FIRST_PAGE_ADDRESS+page_size*page+write_cnt, *(uint64_t*)&data[index*8]);
		  if(status != HAL_OK)
		  {
			  return 1;
		  }
		  write_cnt += 8;
		  index++;
	  }


	  HAL_FLASH_OB_Lock();
	  HAL_FLASH_Lock();
	  return 0;
}

uint32_t save_to_flash(const uint8_t *data,const uint32_t address)
{

	volatile uint64_t data_to_FLASH[(strlen((char*)data)/8)	+ (int)((strlen((char*)data) % 8) != 0)];
	memset((uint8_t*)data_to_FLASH, 0, strlen((char*)data_to_FLASH));
	strcpy((char*)data_to_FLASH, (char*)data);

	volatile uint32_t data_length = (strlen((char*)data_to_FLASH) / 8)
									+ (int)((strlen((char*)data_to_FLASH) % 8) != 0);
	volatile uint16_t pages = (strlen((char*)data)/page_size)
									+ (int)((strlen((char*)data)%page_size) != 0);
	  /* Unlock the Flash to enable the flash control register access *************/
	  HAL_FLASH_Unlock();

	  /* Allow Access to option bytes sector */
	  HAL_FLASH_OB_Unlock();
	  /* Fill EraseInit structure*/
	  FLASH_EraseInitTypeDef EraseInitStruct;
	  EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
	  EraseInitStruct.Banks = FLASH_BANK_1;
	  EraseInitStruct.Page = (address-FIRST_PAGE_ADDRESS)/page_size;
	  EraseInitStruct.NbPages = pages;
	  uint32_t PageError;

	  volatile uint32_t write_cnt=0, index=0;
	  volatile HAL_StatusTypeDef status;

	  status = HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);
	  if(status != HAL_OK)
	  {
		  return PageError;
	  }
	  while(index < data_length)
	  {

		  status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, address+write_cnt, data_to_FLASH[index]);
		  if(status != HAL_OK)
		  {
			  return 1;
		  }
		  write_cnt += 8;
		  index++;
	  }

	  HAL_FLASH_OB_Lock();
	  HAL_FLASH_Lock();
	  return 0;
}
/*data - pointer to data
n_words- 1 word= 4 bytes*/
void read_flash(uint8_t* data,uint32_t address, uint8_t n_words)
{
	volatile uint32_t read_data;
	volatile uint8_t read_cnt=0;

	for(uint8_t i = 0; i < n_words; i++)
	{
		if (read_data == 0xFFFFFFFF){
			break;
		}
			read_data = *(uint32_t*)(address + read_cnt);
			data[read_cnt] = (uint8_t)read_data;
			data[read_cnt + 1] = (uint8_t)(read_data >> 8);
			data[read_cnt + 2] = (uint8_t)(read_data >> 16);
			data[read_cnt + 3] = (uint8_t)(read_data >> 24);
			read_cnt += 4;

	}
}
/*
Examples of writing and reading

char write_data[50];
memset(write_data, 0, sizeof(write_data));
strcpy(write_data, "PRIVET ILIA!!!");
save_to_flash((uint8_t*)write_data,FLASH_STORAGE);
char read_data[50];
memset(read_data, 0, sizeof(read_data));
read_flash((uint8_t*)read_data,FLASH_STORAGE,4);
print_string(read_data);

float write_number = 235.756f;
float *pointer_write = &write_number;
save_to_flash((uint8_t*)pointer_write,FLASH_STORAGE);
float read_number = 0.0f;
float *pointer_read = &read_number;
read_flash((uint8_t*)pointer_read, FLASH_STORAGE,1);
print_float_array(pointer_read,1,"Float read from flash: ");*/
