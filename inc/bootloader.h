#define FIRST_PAGE_ADDRESS    0x08000000
#define page_size 0x800  //2 kbytes
#define BOOTLOADER_IDLE 0
#define BOOTLOADER_READY 1

#define  MAIN_ADRESS 0x08008000
#define  SECOND_ADRESS 0x0800C000
#define  BOOT_FL_ADD 0x08030000

#include "stm32l4xx_hal.h"
#include "ring_buffer.h"
#include "SXC_UHF.h"
uint16_t write_firmware_from_ring(RING_buffer_t *Rx_buf, uint16_t address, uint16_t size);
uint16_t firmware_handler(RING_buffer_t *Rx_buf);
typedef struct
{
    uint8_t  state;
    uint32_t address;
    uint16_t fw_size;           ///
    uint32_t crc32;             ///
} Firmware_set;

typedef void (*pFunction)(void);
