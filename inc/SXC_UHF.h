#include "stm32l4xx_hal.h"
#include "ring_buffer.h"
#define DEVICE_ADDRESS 0x17
#define PARSE_SHORT 0
#define PARSE_LONG  1
#define ACK 0x118
#define NACK 0x119
uint32_t Send_short_msg_UHF(uint16_t address_tx, uint16_t MSG_ID, uint16_t number_of_bytes, uint8_t *TxData);
uint32_t Send_long_msg_UHF(RING_buffer_t* buf, uint16_t address_tx,uint16_t MSG_ID, uint16_t number_of_bytes);
uint32_t Parse_long_msg_UHF(uint8_t *RxData,uint8_t dls,uint8_t start_flag);
uint32_t Parse_short_msg_UHF(uint8_t *RxData,uint8_t dls);
