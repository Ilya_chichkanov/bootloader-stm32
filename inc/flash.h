#include "stm32l4xx_hal.h"
#include "string.h"
#define FIRST_PAGE_ADDRESS    0x08000000
#define FLASH_STORAGE 0x08003000
#define page_size 0x800  //2 kbytes
#define DW_IN_PAGE 256
uint32_t save_to_flash(const uint8_t *data,const uint32_t address);
void read_flash(uint8_t* data,uint32_t address, uint8_t n_words);
uint32_t program_page(const uint8_t *data,const uint16_t page,const uint16_t pack_size);
