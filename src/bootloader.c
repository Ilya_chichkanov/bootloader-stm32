#include "bootloader.h"
extern UART_HandleTypeDef * muart;
uint16_t firmware_size=0;
Firmware_set BOOT = {BOOTLOADER_IDLE,SECOND_ADRESS,0,0};
uint8_t TXdata[8] = {0,0,0,0,0,0,0,0};
uint32_t address,address_last;
uint16_t size;
uint16_t write_firmware_from_ring(RING_buffer_t *Rx_buf, uint16_t address, uint16_t size){
	  volatile uint32_t write_cnt=0,packsize=0;
	  volatile HAL_StatusTypeDef status;
	  while(size>0)
	  {
		  uint8_t data[8] = {0,0,0,0,0,0,0,0};
		  packsize = size>=8 ? 8: size;
		  size-=packsize;
		  RING_PopBuffr(Rx_buf,data,packsize);
		  //HAL_UART_Transmit(muart, data, 8, 0xFF);
		  status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, address+write_cnt, *(uint64_t*)&data[0]);
		  if(status != HAL_OK)
		  {
			  return 1;
		  }
		  write_cnt += 8;
	  }
	  return 0;
}

uint8_t set_bootloader(RING_buffer_t *Rx_buf)
{
    uint32_t address  = RING_Pop32(Rx_buf);
    uint32_t crc32    = RING_Pop32(Rx_buf);
    uint32_t fw_size  = RING_Pop32(Rx_buf);
    BOOT.address = address;
    BOOT.crc32 =  crc32;
    BOOT.fw_size = fw_size;
    BOOT.state = BOOTLOADER_READY;
	HAL_FLASH_Unlock();
	HAL_FLASH_OB_Unlock();
	FLASH_EraseInitTypeDef EraseInitStruct;
	EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
	EraseInitStruct.Banks = FLASH_BANK_1;
	EraseInitStruct.Page = (address-FIRST_PAGE_ADDRESS)/page_size;
	EraseInitStruct.NbPages = (size/page_size)+1;
	uint32_t PageError;
	volatile HAL_StatusTypeDef status;
	status = HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);
	if(status==HAL_OK){
		return 0;
	}
	else{
		return 1;
	}
}
uint16_t firmware_handler(RING_buffer_t *Rx_buf){
	if(BOOT.state==BOOTLOADER_READY){
		    RING_PopBuffr(Rx_buf,TXdata,6);
		    address = TXdata[0] | (TXdata[1] << 8) | (TXdata[2] << 16) | (TXdata[3] << 24);
		    size = ((TXdata[5] << 8) | TXdata[4]);
		    if(address!=address_last){
		    	write_firmware_from_ring(Rx_buf,address,size);
		    	firmware_size+=size;
		    }
		    else{
		    	HAL_UART_Transmit(muart, "POVTOR!!", 8, 0xFF);
		    }
			address_last = address;
			TXdata[2] = (uint8_t)(address >> 0);
			TXdata[3] = (uint8_t)(address >> 8);
			TXdata[4] = (uint8_t)(address >> 16);
			TXdata[5] = (uint8_t)(address >> 24);
			Send_short_msg_UHF(0x1,43523,6,TXdata);
			Send_short_msg_UHF(0x1,ACK,2,TXdata);
			if (firmware_size>=BOOT.fw_size){
				FLASH_EraseInitTypeDef EraseInitStruct;
				EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
				EraseInitStruct.Banks = FLASH_BANK_1;
				EraseInitStruct.Page = (BOOT_FL_ADD-FIRST_PAGE_ADDRESS)/page_size;
				EraseInitStruct.NbPages = 1;
				uint32_t PageError;
			    uint32_t  status = HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);
				uint32_t err = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, BOOT_FL_ADD, 2);
				HAL_FLASH_OB_Lock();
				HAL_FLASH_Lock();
				//uint64_t bootloader_flag_wt[1] = {2};
				//uint32_t err = save_to_flash((uint8_t*){2,0,0,0,0,0,0,0},BOOT_FL_ADD);

				HAL_UART_Transmit(muart, "FINISH  ", 8, 0xFF);
				//BOOT.state = BOOTLOADER_IDLE
			}
	}
	return 0;
}
void JumpToApplication(uint32_t adress)
{
  pFunction  JumpAddress = *(__IO pFunction*)(adress + 4);
  __set_MSP(*(__IO uint32_t*) adress);
  HAL_DeInit();
  JumpAddress();
}

