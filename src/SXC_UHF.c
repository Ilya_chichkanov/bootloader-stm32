#include "stm32l4xx_hal.h"
#include "SXC_UHF.h"
#include "ring_buffer.h"
//#include "bootloader.h"

extern CAN_HandleTypeDef hcan1;
extern UART_HandleTypeDef * muart;
volatile uint16_t MSG_ID_L;
volatile int16_t PCG_LEN;
volatile int16_t lenth_res;
RING_buffer_t ring_Rx;
uint8_t DATA_RX[201];

uint32_t Send_short_msg_UHF(uint16_t address_tx, uint16_t MSG_ID, uint16_t number_of_bytes, uint8_t *TxData){
	CAN_TxHeaderTypeDef   TxHeader;
	uint32_t              TxMailbox[10];
	TxHeader.RTR = CAN_RTR_DATA;
	TxHeader.IDE = CAN_ID_STD;
	TxHeader.DLC = number_of_bytes+2;
	TxHeader.TransmitGlobalTime = DISABLE;
	TxHeader.StdId =(DEVICE_ADDRESS<<5)|(address_tx); //address_rx �� ���� address_tx ����
	TxData[0] = (uint8_t)(MSG_ID & 0x00FF);
	TxData[1] = (uint8_t)((MSG_ID & 0xFF00) >> 8);
	if (HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, TxMailbox) != HAL_OK)
	{
		return hcan1.ErrorCode;
	}
	return 0;
}

uint32_t Parse_short_msg_UHF(uint8_t *RxData,uint8_t dls){
	HAL_UART_Transmit(muart, "SHORT", 8, 0xFF);
	//HAL_UART_Transmit(muart, (uint8_t*)RxData, 8, 1000);
	uint16_t MSG_ID =  ((RxData[1] << 8) | RxData[0]);
	RING_Init(&ring_Rx,DATA_RX, 201);
	RING_PutBuffr(&ring_Rx,RxData+2,dls-2);
	uint32_t res = massage_processor(MSG_ID,ring_Rx);
	return 0;
}

uint32_t Send_long_msg_UHF(RING_buffer_t* buf,uint16_t address_tx,uint16_t MSG_ID, uint16_t number_of_bytes){
	CAN_TxHeaderTypeDef   TxHeader;
	uint32_t              TxMailbox[10];
	TxHeader.RTR = CAN_RTR_DATA;
	TxHeader.IDE = CAN_ID_STD;
	TxHeader.DLC = 6;
	TxHeader.TransmitGlobalTime = DISABLE;
	TxHeader.StdId = 0;
	TxHeader.StdId =(DEVICE_ADDRESS<<5)|(address_tx);
	uint8_t TxData[8];
	TxData[0] = 0xFE;
	TxData[1] = 0xFF ;
	TxData[2] = (uint8_t)(MSG_ID & 0x00FF);
	TxData[3] = (uint8_t)((MSG_ID & 0xFF00) >> 8);
	TxData[4] = (uint8_t)(number_of_bytes & 0x00FF);
	TxData[5] = (uint8_t)((number_of_bytes & 0xFF00) >> 8);
	if (HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, TxMailbox) != HAL_OK)
	{
		return hcan1.ErrorCode;
	}
	TxHeader.StdId |=(0x400);
	uint16_t crc16 = crc16_X_Modern(buf,8,0);
	RING_Put16(buf,crc16);
	number_of_bytes = 8;
	number_of_bytes+=2;

	while(number_of_bytes>=0){
		if(number_of_bytes>=8){
			TxHeader.DLC = 8;
		}
		else {
			TxHeader.DLC = number_of_bytes;
		}

		RING_PopBuffr(buf,TxData,TxHeader.DLC);

		if (HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, TxMailbox) != HAL_OK)
		{
			return hcan1.ErrorCode;
		}
		number_of_bytes-=TxHeader.DLC;
		//HAL_Delay(1);
	}

	return 0;
}

uint32_t Parse_long_msg_UHF(uint8_t *RxData,uint8_t dls,uint8_t start_flag){
	//HAL_UART_Transmit(muart, (uint8_t*)RxData, 8, 1000);
	if(start_flag){
		MSG_ID_L = ((RxData[3] << 8) | RxData[2]);
		PCG_LEN = ((RxData[5] << 8) | RxData[4]);
		lenth_res = PCG_LEN;
		uint8_t temp[4] = {0,0,0,0};
		temp[0] = (uint8_t)((PCG_LEN & 0xFF00) >> 8);
		temp[1] = (uint8_t)(PCG_LEN & 0x00FF);
		HAL_UART_Transmit(muart, (uint8_t*)"LONG", 4, 0xFF);
		HAL_UART_Transmit(muart, temp, 4, 0xFF);
		RING_Init(&ring_Rx,DATA_RX, 201);
	}
	else{
		lenth_res=lenth_res-dls;
		if(lenth_res>0){
			RING_PutBuffr(&ring_Rx,RxData,dls);
		}
		else{
			RING_PutBuffr(&ring_Rx,RxData,dls-2);
			uint16_t crc_houston = ((RxData[dls-1] << 8) | RxData[dls-2]);
			uint16_t crc16 = crc16_X_Modern(&ring_Rx,PCG_LEN-2,0);
			if(crc16==crc_houston){
				uint32_t res = massage_processor(MSG_ID_L,&ring_Rx);
			}
		}

	}
	return 0;
}
